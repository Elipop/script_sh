#!/bin/bash
# Косвенные ссылки на переменные.
 
a=letter_of_alphabet
letter_of_alphabet=z
 
echo
 
# Прямое обращение к переменной.
echo "a = $a"
 
# Косвенное обращение к переменной.
eval a=\$$a
echo "А теперь a = $a"
 
echo
 
 
# Теперь попробуем изменить переменную, на которую делается ссылка.
 
t=table_cell_3
table_cell_3=24
echo "\"table_cell_3\" = $table_cell_3"
echo -n "разыменование (получение ссылки) \"t\" = "; eval echo \$$t
# В данном, простом, случае,
#   eval t=\$$t; echo "\"t\" = $t"
# дает тот же результат (почему?).
 
echo
 
t=table_cell_3
NEW_VAL=387
table_cell_3=$NEW_VAL
echo "Значение переменной \"table_cell_3\" изменено на $NEW_VAL."
echo "Теперь \"table_cell_3\" = $table_cell_3"
echo -n "разыменование (получение ссылки) \"t\" = "; eval echo \$$t
# инструкция "eval" принимает два аргумента "echo" и "\$$t" (назначает равным $table_cell_3)
echo
 
exit 0