#!/bin/bash
 
# Инициализация регистров:
for r in {0..7}
do
	REG[$r]=0
	let r=$r+1
done
# REG[0] питание
#Включаем питание:
REG[0]=1
 
# Инициализация ОЗУ, 32 байт
for t in {0..32}
do
	RAM[$t]=0
	let t=$t+1
done
 
# Запуск
clear
echo -n "mk51"
for x in {0..5}
do
	sleep 1
	echo -n "."
done
 
echo 
 
# Основной цикл
while [ "${REG[0]}"=1 ]
do
	echo -n ">"
	read comm
	RAM[0]=$comm
done
#for a in "${RAM[@]}"; do
#	echo "$a ${n}"
#	let a=$a+1
#done