#!/bin/bash
find . -mtime -1 -type f -print0 | xargs -0 tar rvf "$archive.tar"
exit 0