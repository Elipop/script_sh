#!/bin/bash
find . -mtime -1 -type f -exec tar rvf "$archive.tar" '{}' \;
# более универсальный вариант, хотя и более медленный,
# зато может использоваться в других версиях UNIX.
exit 0