$!/bin/bash
LOG=$0.log
COMMAND1="sleep 100"
echo "Запись в лог всех PID фоновых процессов, запущенных из сценария: $0" >> "$LOG"
# Таким образом возможен мониторинг и удаление процессов по мере необходимости.
echo >> "$LOG"
# Команды записи в лог.
echo -n "PID of \"$COMMAND1\": " >> "$LOG"
${COMMAND1} &
echo $! >> "$LOG"
# PID процесса "sleep 100": 1506
exit 0