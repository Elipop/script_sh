#!/bin/bash
 
#                 rfe
#                 ---
 
# Изменение расширений в именах файлов.
#
#         rfe old_extension new_extension
#
# Пример:
# Изменить все расширения *.gif в именах файлов на *.jpg, в текущем каталоге
#          rfe gif jpg
 
ARGS=2
E_BADARGS=65
 
if [ $# -ne "$ARGS" ]
then
  echo "Порядок использования: `basename $0` old_file_suffix new_file_suffix"
  exit $E_BADARGS
fi
 
for filename in *.$1
# Цикл прохода по списку имен файлов, имеющих расширение равное первому аргументу.
do
  mv $filename ${filename%$1}$2
  #  Удалить первое расширение и добавить второе,
done
 
exit 0