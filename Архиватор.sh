#!/bin/bash
if [ -f cfg_arh ] 				# проверяем наличие конфига 
	then
		true
	else
		echo "Файл конфигурации не найден!!!"
		exit 1
fi
. cfg_arh 					# грузим конфиг
ar_name="$(date +%Y-%m-%d) backup" 		# префикс имени архива
cd $folder_in
echo $(date) " архивация начата">>"$folder_log" # архивируем все файлы в каталоге
for i in *
	do
	tar -cvzf "$folder_out/$ar_name $i.tar.gz" $i &>/dev/null
	echo "файл $i обработан">>"$folder_log"
done
echo $(date)" архивация закончена">>"$folder_log"
echo "------------------------------------------">>"$folder_log"