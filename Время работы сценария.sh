#!/bin/bash
 
TIME_LIMIT=10
INTERVAL=1
 
echo
echo "Для прерывания работы сценария, ранее чем через $TIME_LIMIT секунд, нажмите Control-C."
echo
 
while [ "$SECONDS" -le "$TIME_LIMIT" ]
do
  let "last_two_sym = $SECONDS - $SECONDS / 100 * 100" # десятки и единицы
  if [ "$last_two_sym" -ge 11 -a "$last_two_sym" -le 19 ]
  then
    units="секунд"               # для чисел, которые заканчиваются на "...надцать"
  else
    let "last_sym = $last_two_sym - $last_two_sym / 10 * 10"  # единицы
    case "$last_sym" in
      "1" )
        units="секунду"         # для чисел, заканчивающихся на 1
      ;;
      "2" | "3" | "4" )
        units="секунды"         # для чисел, заканчивающихся на 2, 3 и 4
      ;;
      * )
        units="секунд"          # для всех остальных (0, 5, 6, 7, 8, 9)
      ;;
    esac
  fi
 
  echo "Сценарий отработал $SECONDS $units."
  #  В случае перегруженности системы, скрипт может перескакивать через отдельные
  #+  значения счетчика
  sleep $INTERVAL
done
 
echo -e "\a"  # Сигнал!
 
exit 0